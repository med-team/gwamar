from collections import deque

class ResTreeNode:
    def __init__(self, node_id, parent=None, label=None):
        self.node_id = node_id
        self.children = []
        self.parent = None
        self.e_len = 1.0
        self.ecolor = None
        if label==None: self.label = node_id
        else: self.label = label
        self.contribution = 0.0
    def setLabel(self, label):
        self.label = label
    def addChild(self, child_node):
        self.children.append(child_node)
    def addChildren(self, child_node):
        self.children.append(child_node)
    def setParent(self, parent):
        self.parent = parent
    def is_leaf(self):
        return len(self.children) == 0
    def getLeaves(self):
        queue = deque([])
        if len(self.children) == 0: return [self]
        queue.append(self)
        
        leaves = []
        while queue:
            node = queue.popleft()
            if node == None: continue
            children_list = list(node.children)
            if len(children_list) == 0: leaves.append(node)
            else:
                for child in children_list: queue.append(child)
        return leaves

    def flatten(self, thr = 0.0, min_val = 0.00001):
        if self.e_len > thr:
            curr_node = ResTreeNode(self.node_id)
            curr_node.e_len = max(min_val, self.e_len)
            for child_node in self.children:
                res = child_node.flatten(thr)
                for c in res:
                    #c.setEdgeLen(child_node.getEdgeLen())
                    curr_node.children.append(c)
            return [curr_node]
        else:
            if len(self.children) == 0:
                leaf_node = ResTreeNode(self.node_id)
                leaf_node.e_len = max(min_val,self.e_len)
                return [leaf_node]
            else:
                forest = []
                for child_node in self.children:
                    res = child_node.flatten(thr)
                    for c in res:
                        forest.append(c)        
                return forest;
            
    def toString(self, lengths=True, is_root = True, strains = None, int_labels= False, strains_map={}):
        n = len(self.children)
        if n==0:
            if lengths == False:
                if strains == None:
                    return str(self.label)
                else:
                    strain_id = str(strains.getStrain(self.node_id))
                    return strains_map.get(strain_id, strain_id)
            else:
                if strains == None:
              #      print("ttt", strain_id, strains_map)
                    return str(strains_map.get(self.label, self.label)) + ":" + str("%.5f" % self.e_len)
                else:
                    strain_id = str(strains.getStrain(self.node_id))
              #      print("xxx", strain_id, strains_map)
                    return  strains_map.get(strain_id, strain_id) + ":" + str("%.5f" % self.e_len)
        else:
            text = "("
            i = 0
            for child in self.children:
                text += child.toString(lengths = lengths, is_root = False, strains = strains, int_labels=int_labels, strains_map=strains_map)
                i += 1
                if i < n:
                    text += ","
            text += ")"
            if int_labels == True:
                text +="S" + str(self.node_id)
                
            if lengths == True and is_root == False:
                text += ":" + str("%.5f" % self.e_len)
            return text

class ResTree:
    def __init__(self, leaves_count):
        self.root_id = 0
        self.root = None
        self.nodes = {}
        self.leaves_count = leaves_count
        self.nodes_count = leaves_count
        self.leaves = set([])
        self.leaf_ids = set([])
        
    def calcInnerNodesBU(self):
        for node in self.nodes.values():
            node.visited = 0
        
        q = deque(self.leaves)
        
        self.inner_nodes_bu = []
        while len(q) > 0:
            node = q.popleft()
            if len(node.children) > 0:
                self.inner_nodes_bu.append(node)
                
            parent = node.parent
            if parent != None:
                parent.visited += 1
                if parent.visited == len(parent.children):
                    q.append(parent)
            
        return self.inner_nodes_bu
            
    def flatten(self, thr=0.0):
        flatten_forest = self.root.flatten(thr)
        flat_root = None
        if len(flatten_forest) == 1:
            flat_root = list(flatten_forest)[0]
        else:
            flat_root = ResTreeNode("x")
            for c in flatten_forest:
                flat_root.addChild(c)
        leaves_count = len(flat_root.getLeaves())
        flat_tree = ResTree(leaves_count)
        flat_tree.setRoot(flat_root)
        return flat_tree
    
    def setObservedResProfile(self, res_profile):
        for i in set(range(len(res_profile))) & set(self.nodes.keys()):
            self.nodes[i].observed_res_state = res_profile[i]

    def setObservedResProfileOther(self, res_profiles):
        k = len(res_profiles)
        for i in set(self.nodes.keys()):
            self.nodes[i].observed_res_state_map = ["?"]*k
        for di in range(k):
            for i in set(range(len(res_profiles[di]))) & set(self.nodes.keys()):
            #    if not i in res_profiles[di]:
            #        self.nodes[i].observed_res_state_map[di] = "?"
            #        print("xxx", i, di, res_profiles[di])
            #    else:
            #        print("here")
                self.nodes[i].observed_res_state_map[di] = res_profiles[di][i]
            
    def setObservedResProbProfile(self, res_profile):
        for i in set(res_profile.keys()) & set(self.nodes.keys()):
            self.nodes[i].observed_res_prob_state = res_profile[i]
            
    def setObservedMutProfile(self, mut_profile):
        for i in set(range(len(mut_profile))) & set(self.nodes.keys()):
            self.nodes[i].observed_mut_state = mut_profile[i]
            
    def setComepleteResProfile(self, res_profile):
        for i in set(range(len(res_profile))) & set(self.nodes.keys()):
            self.nodes[i].complete_res_state = res_profile[i]
            
    def setComepleteMutProfile(self, mut_profile):
        for i in set(range(len(mut_profile))) & set(self.nodes.keys()):
            self.nodes[i].complete_mut_state = mut_profile[i]
            
    def setLeavesByNodes(self, leaves):
        self.leaves = leaves 
        for leaf in leaves:
            self.leaf_ids.add(leaf.node_id)
    def setLeavesByIDs(self, leaf_ids):
        self.leaf_ids = leaf_ids
        for leaf_id in leaf_ids:
            self.leaves.add(self.nodes[leaf_id])
            
    def setRoot(self, root):
        self.root_id = root.node_id
        self.root = root
        
    def addLeafNode(self, node):
        self.nodes[node.node_id] = node
        
    def addInternalNode(self, node):
        self.nodes[node.node_id] = node
        self.nodes_count += 1
        
    def createNodeID(self, node_name):
        return self.nodes_count
    
    def calcTreeSupport(self, mutation, drug_res_profile, ext=False):
        root = self.root
        changes = self.calcRecTreeChanges(root, mutation, drug_res_profile, mutation.ref_state, ext)
        return changes
    
    def calcWeightedSupport(self, mutated):
        cts = 0.0
        for leaf_node in self.leaves:
            if leaf_node.node_id in mutated: cts += leaf_node.contribution
        return cts
    
    def maximalWeightedSupport(self):
        max_score = 0.0
        for leave_node in self.leaves:
            max_score += max(0, leave_node.contribution)
        return max_score
    
    def setContribiutionsRec(self, contr, res_profile, node):
        if node.node_id in self.leaf_ids and res_profile.full_profile[node.node_id] == "R":
            node.contribution = contr
        else:
            for child_node in node.children:
                self.setContribiutionsRec(contr, res_profile, child_node)
                
    def calcContributionsRec(self, res_profile, node, beta):
        if node.node_id in self.leaf_ids:
            rs = res_profile.full_profile[node.node_id]
            if rs == "R": 
                return 1
            elif rs == "S":
                node.contribution = -beta
                return None
            else:
                node.contribution = 0.0
                return 0
        else:
            all_res = True
            res_map = {}
            sum_res = 0
            for c_node in node.children:
                res_c = self.calcContributionsRec(res_profile, c_node, beta)
                if res_c == None:
                    all_res = False
                else:
                    res_map[c_node] = res_c
                    sum_res += res_c
            if all_res:
                return sum_res
            else:
                for c_node in res_map:
                    res_r = res_map[c_node]
                    if res_r >  0.0:
                        self.setContribiutionsRec(1.0/float(sum_res), res_profile, c_node)
                return None                        

    def calcContributions(self, res_profile, beta):
        self.calcContributionsRec(res_profile, self.root, beta)
        
    def calcRecTreeChanges(self, node, mutation, drug_res_profile, state_x, ext=False):
        if node.node_id in self.leaf_ids:
            ms = mutation.getState(node.node_id)
            rs = drug_res_profile.getState(node.node_id)
            if ms == state_x or ms == "?":
                return 0
            else:
                if rs == "R":
                    return 1
                elif rs == "?" and ext:
                    return 1
                else:
                    return 0
        else:
            changes = 0
            for child_node in node.children:
                min_c = (-1)
                for state_y in mutation.diff_states:
                    m_c = self.calcRecTreeChanges(child_node, mutation, drug_res_profile, state_y, ext)
                    if min_c == -1 or m_c < min_c:
                        min_c = m_c
                changes += min_c  
        return changes;
    
    def setResMutation(self, bin_profile, res_profile):
        res_strains = res_profile.getResistant()
        for strain_id in self.leaf_ids:
            tree_node = self.nodes[strain_id]
            if bin_profile[strain_id] == '0':
                tree_node.n_zero = 0
                tree_node.n_one = 1
            elif bin_profile[strain_id] == '1' and strain_id in res_strains:
                tree_node.n_zero = 1
                tree_node.n_one = 0                
            else:
                tree_node.n_zero = 0
                tree_node.n_one = 0
                
    def setFullMutation(self, bin_profile, res_profile):
        for strain_id in self.leaf_ids:
            tree_node = self.nodes[strain_id]
            if bin_profile[strain_id] == '0':
                tree_node.n_zero = 0
                tree_node.n_one = 1
            elif bin_profile[strain_id] == '1':
                tree_node.n_zero = 1
                tree_node.n_one = 0                
            else:
                tree_node.n_zero = 0
                tree_node.n_one = 0
                                
    def calcChanges(self):
        root = self.nodes[self.root_id]
        n_zero, n_one = self.calcChangesRec(root)
        #return min(n_zero, n_one)
        return min(n_zero,n_one)
    def getText(self):
        text = ""
        for strain_id in range(0, self.nodes_count, 1):
            text += str(strain_id)+" "+ str(self.nodes[strain_id].n_zero)+" "+ str(self.nodes[strain_id].n_one)
            text += " children: ";
            node = self.nodes[strain_id]
            for child in node.children:
                text += str(child.node_id)+",";
            text += "\n"
        return text
    
    def calcChangesRec(self, node):
        if node.node_id in self.leaf_ids:
            return node.n_zero, node.n_one
        else:
            n_zeros = []
            n_ones = []
            for subnode in node.children:
                #subnode = self.nodes[subnode_id]
                n_zero, n_one = self.calcChangesRec(subnode)
                n_zeros.append(n_zero)
                n_ones.append(n_one)
            zero_yes = False
            one_yes = False
            for i in range(0, len(n_zeros), 1):
                if n_zeros[i] > n_ones[i]:
                    zero_yes = True 
                if n_ones[i] > n_zeros[i]:
                    one_yes = True
            if zero_yes:
                n_zero_res = 1
            else:
                n_zero_res = 0
            if one_yes:
                n_one_res = 1
            else:
                n_one_res = 0
            for i in range(0, len(n_zeros), 1):
                n_zero_res += min(n_zeros[i], n_ones[i])
                n_one_res += min(n_zeros[i], n_ones[i])
            
            node.n_zero = n_zero_res
            node.n_one = n_one_res
            return n_zero_res, n_one_res

    def toString(self, lengths=True, strains = None, int_labels = False, strains_map={}):
        return self.root.toString(is_root = True, lengths = lengths, strains=strains, int_labels=int_labels, strains_map=strains_map)
            
    def __str__(self):
        txt = "TREE ROOT ID: " + str(self.root_id) + "\n"
        for i in self.nodes:
            node_i = self.nodes[i]
            parent = node_i.parent
            if parent == None: 
                txt += str(i) + "\t" + str("ROOT:")+str(node_i.ecolor)+"|"+str("%.5f" % 0.0)+"\t" #+ str(len(node_i.children)) +":"
            else:
                txt += str(i) + "\t" + str(parent.node_id) + ":"+str(node_i.ecolor)+"|"+str("%.5f" % node_i.e_len)+"\t" #+ str(len(node_i.children)) +":"
            for node_j in node_i.children:
                txt += str(node_j.node_id) + ","
            txt = txt[:-1] + "\n"
             
        return txt
            

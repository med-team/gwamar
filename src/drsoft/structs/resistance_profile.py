class ResistanceProfile:
    def __init__(self, drug_name, count=None, full_profile=None):
        self.drug_name = drug_name
        if full_profile == None:
            self.count = count
            self.sc_strains_ids = set([])
            self.inter_strains_ids = set([])
            self.res_strains_ids = set([])
            self.known_strains_ids = set([])
            self.unknown_strains_ids = set([])
            self.full_profile = ['?']*count
            self.mic_full_profile = [0.0]*count
            self.prob_full_profile = [0.0]*count
            self.pred_strains = set([])
        else:
            self.count = len(full_profile)
            self.full_profile = full_profile
            self.computeResSets()
        
    def getFullKey(self):
        full_key = ''.join(self.full_profile)
        return full_key  
    
    def computeResSets(self):
        self.sc_strains_ids = set([])
        self.inter_strains_ids = set([])
        self.res_strains_ids = set([])
        self.known_strains_ids = set([])
        self.unknown_strains_ids = set([])
        for i in range(self.count):
            state = self.full_profile[i]
            if state == "R":
                self.res_strains_ids.add(i)
                self.known_strains_ids.add(i)
            elif state == "S":
                self.sc_strains_ids.add(i)
                self.known_strains_ids.add(i)
            elif state == "I":
                self.inter_strains_ids.add(i)
                self.known_strains_ids.add(i)
            else:
                self.unknown_strains_ids.add(i)
                
    def changeProbState(self, strain_index, prob):
        self.prob_full_profile[strain_index] = prob
        
    def changeState(self, strain_index, state):
        self.full_profile[strain_index] = state
        
    def size(self):
        return self.count
        
    def getSusceptible(self):
        return self.sc_strains_ids       
    def getResistant(self):
        return self.res_strains_ids
    def getInterresistant(self):
        return self.inter_strains_ids
    def getUnknown(self):
        return self.unknown_strains_ids      
    def getKnown(self):
        return self.known_strains_ids

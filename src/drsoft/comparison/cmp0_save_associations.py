import sys
import os
import multiprocessing
from multiprocessing import Pool

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_scoring,\
  gwamar_res_utils, gwamar_utils, gwamar_strains_io_utils, gwamar_res_io_utils,\
  gwamar_bin_utils, gwamar_ann_utils, gwamar_muts_io_utils,\
  gwamar_progress_utils

def readGeneSet(input_fh):
    lines = input_fh.readlines()
    genes_set = set([])
    for line in lines:
        line = line.strip()
        genes_set.add(line)
    return genes_set


def printGoldShort(gold_assocs, comment=""):
    for drug_name in gold_assocs:
        tmp = gold_assocs[drug_name].keys()
        tl = comment+" " +drug_name +" "
        for gene_id in tmp:
            tl += gene_id +"("+str(len(gold_assocs[drug_name][gene_id]))+"),"

        print(tl.strip(","))

def readScoresMap(input_fh):
    scores_map = {}
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2: continue
        bin_profile_id = tokens[0]
        score = float(tokens[1])
        scores_map[bin_profile_id] = score
    return scores_map

def mutationsIDmap(bin_profile_details):
    mutations_map = {}
    mut_key_id = 0

    for bin_profile_id in bin_profile_details:
        for (gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt) in bin_profile_details.get(bin_profile_id, []):
            mut_key = " ".join([gene_id, cluster_id, gene_name, str(position), res_states_txt, mut_states_txt])
            mutations_map[mut_key] = mut_key_id
            mut_key_id += 1
    return mutations_map

def readScoreMutations(bin_profile_details, scores_bin_map):
    n = len(scores_bin_map)
    if n == 0: return []
    mut_scores = {}

    for bin_profile_id in scores_bin_map:
        bin_profile = bin_profiles.get(bin_profile_id, None)
        if bin_profile == None: continue

        for (gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt) in bin_profile_details.get(bin_profile_id, []):
            mut_key = " ".join([gene_id, cluster_id, gene_name, str(position), res_states_txt, mut_states_txt])
            score = scores_bin_map.get(bin_profile_id, 0.0)
            mut_scores[(mut_key, bin_profile_id)] = score

    return mut_scores

def goldAssociations(bin_profile_details, drug_name):
    mut_assocs = {}
    for bin_profile_id in bin_profile_details:
        for (gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt) in bin_profile_details.get(bin_profile_id, []):
            if cluster_id in drug_res_all_db.get(drug_name,[]) and position in drug_res_all_db[drug_name][cluster_id]:
                db_all = "Y"
            elif gene_id in drug_res_all_db.get(drug_name,[]) and position in drug_res_all_db[drug_name][gene_id]:
                db_all = "Y"
            elif cluster_id in drug_res_conf_db.get(drug_name,[]) and position in drug_res_conf_db[drug_name][cluster_id]:
                db_all = "Y"
            elif gene_id in drug_res_conf_db.get(drug_name,[]) and position in drug_res_conf_db[drug_name][gene_id]:
                db_all = "Y"
            else: db_all = "N"
            
            if cluster_id in drug_res_conf_db.get(drug_name,[]) and position in drug_res_conf_db[drug_name][cluster_id]:
                db_hc = "Y"
            elif gene_id in drug_res_conf_db.get(drug_name,[]) and position in drug_res_conf_db[drug_name][gene_id]:
                db_hc = "Y"
            else: db_hc = "N"

            if cluster_id in drug_res_conf_db.get(drug_name,[]) and len(set([position,position-1,position+1]) & set(drug_res_conf_db[drug_name][cluster_id].keys()))>0:
                db_neigh_hc = "Y"
            elif gene_id in drug_res_conf_db.get(drug_name,[]) and len(set([position,position-1,position+1]) & set(drug_res_conf_db[drug_name][gene_id].keys()))>0:
                db_neigh_hc = "Y"
            else: db_neigh_hc = "N"

          #  if db_neigh_hc == "Y" and db_hc == "N":
          #      print(gene_id, cluster_id, gene_name, position, res_states_txt, mut_states_txt)
            if cluster_id in drug_res_all_db.get(drug_name,[]) or gene_id in drug_res_all_db.get(drug_name,[]):
                db_gene_all = "Y"
            elif cluster_id in drug_res_conf_db.get(drug_name,[]) or gene_id in drug_res_conf_db.get(drug_name,[]):
                db_gene_all = "Y"
            else: db_gene_all = "N"

            if cluster_id in drug_res_conf_db.get(drug_name,[]) or gene_id in drug_res_conf_db.get(drug_name,[]):
                db_gene_high = "Y"
            else: db_gene_high = "N"

            if gene_id in broad_genes: broad_gene_q = "Y"
            else: broad_gene_q = "N"

            mut_key = " ".join([gene_id, cluster_id, gene_name, str(position), res_states_txt, mut_states_txt])

            mut_assocs[(drug_name, mut_key)] = "\t".join([db_all, db_hc, db_neigh_hc, db_gene_all, db_gene_high, broad_gene_q])
    
    return mut_assocs

def saveAssociations(params):

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    scores_dir = parameters["RESULTS_SCORES_DIR"]
    cmp_assocs_dir = parameters["RESULTS_CMP_ASSOCS_DIR"]

    score, gold_assocs, mutations_map, drug_names_map = params
    
    statistics = {}
    associations = {}

    revord = gwamar_scoring.revOrder(score)
                     
    for drug_res_profile in drug_res_profiles:
        dr_profile = drug_res_profile.full_profile
        drug_name = drug_res_profile.drug_name

        bin_profiles_res_rel = bin_profile_details

        input_fn = scores_dir + "/" + drug_name  + "/" + score + ".txt"
        if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 10:
            print("NOT EXIST: ", input_fn)
            output_fn = cmp_assocs_dir + "/" + score + ".txt"
            if os.path.exists(output_fn):
                os.remove(output_fn)
            return score
        else:
            input_fh = open(input_fn)
            bin_scores_map = readScoresMap(input_fh)
            input_fh.close()
            

        mut_scores = readScoreMutations(bin_profiles_res_rel, bin_scores_map)
        drug_name_id = drug_names_map[drug_name]
        for (mutkey, bin_profile_id) in mut_scores:
            mutkey_id = mutations_map[mutkey]
            bin_profile = bin_profiles[bin_profile_id]

            associations[(drug_name_id, mutkey_id)] = mut_scores[(mutkey, bin_profile_id)]

            s0, s1, i0, i1, r0, r1 = gwamar_res_utils.compareProfiles(bin_profile, dr_profile)  
            statistics[(drug_name_id, mutkey_id)] = "\t".join(map(str, [s0,s1,i0,i1,r0,r1]))

    associations_sorted = sorted(associations, key=lambda assoc_key: revord*associations[assoc_key])
            
    output_fn = cmp_assocs_dir + "/" + score + ".txt"    
    output_fh = open(output_fn, "w")
    for (drug_name_id, mutkey_id) in associations_sorted:
        score_val = associations[(drug_name_id, mutkey_id)]
        stats_tmp = statistics[(drug_name_id, mutkey_id)]
        gold_assoc = gold_assocs[(drug_name_id, mutkey_id)]

        tl = "\t".join(map(str, [drug_name_id, mutkey_id, gold_assoc, score_val, stats_tmp]))
        output_fh.write(tl + "\n")
    output_fh.close()
    return score

if __name__ == '__main__':
    print("cmp0_save_associations.py: saves association lists.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = parameters["D"]
    input_dir = parameters["DATASET_INPUT_DIR"]
    gold_dir = parameters["DATASET_GOLD_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    ranks_dir = parameters["RESULTS_RANKS_DIR"]
    cmp_dir = parameters["RESULTS_CMP_DIR"]
    cmp_assocs_dir = parameters["RESULTS_CMP_ASSOCS_DIR"]

    gwamar_utils.ensure_dir(cmp_dir)
    gwamar_utils.ensure_dir(cmp_assocs_dir)

    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    scores_p = parameters["SCORES"].split(",")

    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    m_type = parameters["MT"]
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_res_profiles = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()

    input_fh = open(results_dir + 'bin_profiles_' + m_type + '_det.txt')
    bin_profile_details = gwamar_bin_utils.readBinProfileDetails(input_fh, strains.count(), mut_type=m_type, out_format="tuple")
    input_fh.close()

    input_fh = open(results_dir + 'bin_profiles_' + m_type + '.txt')
    bin_profiles = gwamar_bin_utils.readBinProfiles(input_fh, strains.count(), gen_bin_profiles=True)
    input_fh.close()
    
    input_fn = input_dir + "/cluster_gene_ref_ids.txt"
    if os.path.exists(input_fn):
        cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
        cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh, gene_ref_ids=True)
        cluster_fh.close()
    else:
        cluster_ids = {} 
        cluster_ids_rev = {}

    input_fn = gold_dir + "/gold_assocs_H.txt"
    if not os.path.exists(input_fn):
        print("File does not exist:" + input_fn)
        drug_res_conf_db = {}
    else:
        input_fh = open(input_fn)
        drug_res_conf_db = gwamar_muts_io_utils.readGoldAssociations(input_fh, cluster_ids_rev, drug_names=drug_names)
        input_fh.close()
    
    input_fn = gold_dir + "/gold_assocs_A.txt"
    if not os.path.exists(input_fn):
        print("File does not exist:" + input_fn)
        drug_res_all_db = {}
    else:
        input_fh = open(input_fn)
        drug_res_all_db = gwamar_muts_io_utils.readGoldAssociations(input_fh, cluster_ids_rev, drug_names=drug_names)
        input_fh.close()

    input_fn = gold_dir + "/broad_genes.txt"
    if not os.path.exists(input_fn):
        broad_genes = set([])
    input_fh = open(input_fn)
    broad_genes = readGeneSet(input_fh)
    input_fh.close()

    score_params_union = set([])    
    drug_names_map = {}
    drug_name_id = 0
    for drug_res_profile in drug_res_profiles:
        drug_name = drug_res_profile.drug_name

        for score_fn in os.listdir(scores_dir + "/" + drug_name):
            input_fn = scores_dir + "/" + drug_name  + "/" + score_fn
            if os.path.exists(input_fn): 
                score_tmp = score_fn[:-4]
                if score_tmp in scores_p:
                    score_params_union.add(score_tmp)
                if not drug_name in drug_names_map:
                    drug_names_map[drug_name] = drug_name_id
                    drug_name_id += 1

    print(','.join(drug_names_map.keys()))
    printGoldShort(drug_res_all_db, "all")
    printGoldShort(drug_res_conf_db, "hc")

    mutations_map = mutationsIDmap(bin_profile_details)

    output_fn = cmp_dir + "/mutations_map.txt"
    output_fh = open(output_fn, "w")
    for mutkey in mutations_map:
        mutkey_id = mutations_map[mutkey]
        output_fh.write(str(mutkey_id) + "\t" + str(mutkey) + "\n")
    output_fh.close()

    output_fn = cmp_dir + "/drug_names_map.txt"    
    output_fh = open(output_fn, "w")
    for drug_name in drug_names_map:
        drug_name_id = drug_names_map[drug_name]
        output_fh.write(str(drug_name_id) + "\t" + str(drug_name) + "\n")
    output_fh.close()

    gold_assocs = {}

    for drug_res_profile in drug_res_profiles:
        dr_profile = drug_res_profile.full_profile
        drug_name = drug_res_profile.drug_name
        gold_assoscs_tmp = goldAssociations(bin_profile_details,drug_name)
        for (drug_name, mutkey) in gold_assoscs_tmp:
            drug_name_id = drug_names_map[drug_name]
            mutkey_id = mutations_map[mutkey]
            gold_assocs[(drug_name_id, mutkey_id)] = gold_assoscs_tmp[(drug_name, mutkey)]

    TASKS = []
    for score in list(score_params_union):
        if os.path.exists(cmp_assocs_dir + "/" + score + ".txt") and parameters.get("OW")=="N":
            continue
        TASKS.append((score, gold_assocs, mutations_map, drug_names_map))

    WORKERS = min(WORKERS, len(TASKS))

    progress = gwamar_progress_utils.GWAMARProgress("Associations")
    progress.setJobsCount(len(TASKS))
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveAssociations, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveAssociations(T)
            progress.update(str(r))

    print("cmp0_save_associations.py: Finished.")

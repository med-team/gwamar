import sys
import os
import multiprocessing
from multiprocessing import Pool

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_res_io_utils,\
  gwamar_scoring, gwamar_bin_utils, gwamar_muts_utils, gwamar_ann_utils,\
  gwamar_muts_io_utils
from src.drsoft.utils.gwamar_utils import ensure_dir
from src.drsoft.utils.gwamar_res_utils import compareProfiles
from src.drsoft.utils.gwamar_strains_io_utils import readStrains
from src.drsoft.utils.gwamar_progress_utils import GWAMARProgress

def saveScoresCombined(params):
    drug_name = params["DRUG_NAME"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    input_dir = parameters["DATASET_INPUT_DIR"]
    gold_dir = parameters["DATASET_GOLD_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    show_profiles = parameters.get("SP", "B")
    score_sort = parameters.get("SCORE_SORT", "r-tgh")

    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = readStrains(input_fh)
    input_fh.close()

    score_params = list(parameters["SCORES"].split(","))
    

    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    res_profiles = {}
    for res_profile in res_profiles_list:
        res_profiles[res_profile.drug_name] = res_profile
    drp = res_profiles[drug_name]

    bin_profile_ids = set([])
    score_map = {}


    for score in score_params:
        score_map[score] = {}
        input_fn = scores_dir + drug_name + "/" + score + ".txt"
        if not os.path.exists(input_fn):
            continue     
        input_fh = open(input_fn) 
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 2: continue
            bin_profile_id = tokens[0]
            score_val = tokens[1]
            bin_profile_ids.add(bin_profile_id)
            score_map[score][bin_profile_id] = score_val
        input_fh.close()
    
    revord = gwamar_scoring.revOrder(score_sort)

    bin_profile_ids_sorted = sorted(bin_profile_ids, key=lambda bin_profile_id:(
                    revord*float(score_map.get(score_sort,{}).get(bin_profile_id,0.0))
                    ))

    input_fh = open(results_dir + '/bin_profiles_' + parameters["MT"] + '.txt')         
    bin_profiles = gwamar_bin_utils.readBinProfiles(input_fh, strains.count(), gen_bin_profiles=True, decompress=True)
    input_fh.close()

    input_fh = open(results_dir + '/bin_profiles_' + parameters["MT"] + '_det.txt')
    bin_profile_details = gwamar_bin_utils.readBinProfileDetails(input_fh, strains.count(), parameters["MT"], out_format="tuple", ref_counts=True)
    input_fh.close()

    if os.path.exists(input_dir + "/cluster_gene_ref_ids.txt"):
        cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
        cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh, gene_ref_ids=True)
        cluster_fh.close()
    else:
        cluster_ids = {}
        cluster_ids_rev = {}
    
    input_fn = gold_dir + "/gold_assocs_H.txt"

    if not os.path.exists(input_fn):
        print("No file: " + input_fn)
        exit()

    input_fh = open(input_fn)
    drug_res_rels = gwamar_muts_io_utils.readGoldAssociations(input_fh, cluster_ids_rev, genes_level=True)#loadDrugResRels(input_fh, cluster_ids_rev, genes_level=True)
    input_fh.close()

    drug_res_rels["Rifampicin"]["Rv0668"] = None
    drug_res_rels["Rifampicin"]["Rv3457c"] = None

    bin_profiles_res_rel = gwamar_bin_utils.selectResRelBinProfiles(bin_profile_details, drug_res_rels.get(drug_name, {}), cluster_ids, cluster_ids_rev)
    
    tls = []
    
    dr_profile = drp.getFullKey()
    
    if show_profiles in ["B","Y"]:
        tl = dr_profile + "\t"
    elif show_profiles == "K":
        tl = ''.join(gwamar_muts_utils.profileSubset(drp.full_profile, drp.getKnown())) + "\t"
    elif show_profiles == "R":
        tl = ''.join(gwamar_muts_utils.profileSubset(drp.full_profile, drp.getResistant() | drp.getInterresistant())) + "\t"
    else: 
        tl = ""
    
    tl += "S0\tS1\tI0\tI1\tR0\tR1\t"
    tl += "profile_id\t"
    tl += "\t".join(score_params) + "\t"
    tl += "details" + "\n"
    tls.append(tl)
        
    for i in range(len(bin_profile_ids_sorted)):
        bin_profile_id = bin_profile_ids_sorted[i]
        if not bin_profile_id in bin_profiles_res_rel:
            continue
        bin_profile = bin_profiles[bin_profile_id]
        mut_details = gwamar_bin_utils.binProfilesDetailsToStr(bin_profiles_res_rel[bin_profile_id])

        s0, s1, i0, i1, r0, r1 = compareProfiles(bin_profile, dr_profile) 
        
        if show_profiles in ["B","Y"]:
            tl = ''.join(bin_profile) + "\t"
        elif show_profiles == "K": 
            tl = ''.join(gwamar_muts_utils.profileSubset(bin_profile, drp.getKnown())) + "\t"
        elif show_profiles == "R":
            tl = ''.join(gwamar_muts_utils.profileSubset(bin_profile, drp.getResistant() | drp.getInterresistant())) + "\t"
        else: tl = ""


        tl += "\t".join(map(str, [s0,s1,i0,i1,r0,r1])) + "\t"
        tl += str(bin_profile_id) + "\t"
        
        for score in score_params:
            if not score in score_map:
                tl += "NA"
            elif not bin_profile_id in score_map[score]:
                tl += "NA"
            else:
                tl += score_map[score][bin_profile_id]
            tl += "\t"

        tl += mut_details + "\n"
        tls.append(tl)

    output_fh = open(analysis_comb_dir + drug_name + ".txt", "w") 
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()
    
    return drug_name
    
if __name__ == '__main__':
    print("a2_save_details_scores_sel.py: Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    
    ensure_dir(analysis_dir)
    ensure_dir(analysis_comb_dir)
    print("Save to: " + analysis_comb_dir)

    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        TASK["DRUG_NAME"] = drug_name
        TASKS.append(TASK.copy())

    progress = GWAMARProgress("Associations selected (drug specific)")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveScoresCombined, TASKS):
            progress.update("r:"+str(r))
    else:
        for T in TASKS:
            r = saveScoresCombined(T)
            progress.update("r:"+str(r))

    print("a2_save_details_scores_sel.py: Finished.")

import sys
import os
from multiprocessing import Pool
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_res_io_utils,\
  gwamar_progress_utils

def scoreDrug(params):
    score_full = params["SCORE"]
    drug_name = params["DRUG_NAME"]
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    scores_dir = parameters["RESULTS_SCORES_DIR"]
    perm_pvalues_dir = parameters["RESULTS_PERM_PVALUES_DIR"]
    perm_rep = parameters["REPS"]
    
    input_fn = perm_pvalues_dir + drug_name + "/" + score_full +  ".txt"
    if not os.path.exists(input_fn):
        print(input_fn)
        return drug_name, score_full
    
    text_lines = []
    
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2: continue
        text_line = tokens[0] + "\t" + tokens[1] + "\n"
        text_lines.append(text_line)
    input_fh.close()

    output_fh = open(scores_dir + drug_name + "/" + "pv-" + str(perm_rep) + "-" + score_full + ".txt", "w") 
    for text_line in text_lines:
        output_fh.write(text_line)
    output_fh.close()
    
    return drug_name, score_full
    
if __name__ == '__main__': 
    print("sc3_compute_perm_scores.py. Permutation scores.")
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    score_params = parameters["SCORES"].split(",")
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    
    TASKS = []
    TASK = {}

    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        for score_param in score_params:

            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score_param

            TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Scores computed")
    progress.setJobsCount(len(TASKS))

    WORKERS = min(WORKERS, len(TASKS))
    
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(scoreDrug, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreDrug(T)
            progress.update(str(r))

    print("sc3_compute_perm_scores.py. Finished.")

import os
import sys
from src.drsoft.utils import gwamar_params_utils, gwamar_utils,\
  gwamar_strains_io_utils, gwamar_res_io_utils

def readCompMutations(input_fh):
    comp_mutations = {}
    source = ""
    gene_name = ""

    lines = input_fh.readlines()
    for line in lines:
        line = line.strip()
        line = line.split("#")[0].strip()
        if len(line) == 0:
            continue
        if line.startswith("%"):
            source = line[1:]
            comp_mutations[source] = {}
        elif line.startswith(">"):
            gene_name = line[1:].split()[0]
            comp_mutations[source][gene_name] = {}
        else:
            tokens = line.split()
            if len(tokens) < 3:
                continue
            position = int(tokens[0].split("|")[0])
            ref_state = tokens[1]
            comp_mutations[source][gene_name][(position, ref_state)] = []
            mut_state_tokens = tokens[2].split(",")
            for mut_state_token in mut_state_tokens:
                mut_tokens = mut_state_token.split(":")
                mut_state = mut_tokens[0]
                if len(mut_tokens) <= 1: mut_state_count = 1
                else: mut_state_count = int(mut_tokens[1])
                comp_mutations[source][gene_name][(position, ref_state)].append((mut_state, mut_state_count))
    return comp_mutations


gene_shift = {"rpoA":50, "rpoB":300, "rpoC": 550}

gene_length = {"rpoA": 347, "rpoB": 1172, "rpoC": 1314}

source_color = {"comas_whole-genome_2012": "red", "de_vos_putative_2013":"blue", "casali_evolution_2014":"black",
 "mtu173":"orange", "mtu_broad":"violet", "brandis_fitness-compensatory_2012": "pink",
 "brandis_genetic_2013": "purple"}
source_shifts = {"comas_whole-genome_2012": 0, "de_vos_putative_2013": 10, "casali_evolution_2014": 20,
        "mtu173":30, "mtu_broad":40, "brandis_fitness-compensatory_2012":50, "brandis_genetic_2013":60
        }

x_offset = 5

if __name__ == '__main__':
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    stats_dir = parameters["STATS_DIR"]
    latex_dir = parameters["LATEX_DIR"]
    comp_dir = parameters["COMP_DATA"]

    gwamar_utils.ensure_dir(stats_dir)
    gwamar_utils.ensure_dir(latex_dir)

    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()

    input_fh = open(comp_dir + "/comp_data.txt")
    comp_mutations = readCompMutations(input_fh)
    input_fh.close()

    res_profiles_sorted = sorted(res_profiles_list, key=lambda drp: -len(drp.getKnown()))

    tls = []
    tls.append("\\documentclass[10pt]{article}\n")
    tls.append("\\usepackage{tikz}\n")
    #tls.append("\\tikzset{user/.style={font=\\bfseries}}\n");
    #tls.append("\\usepackage[landscape]{geometry}\n")
    tls.append("\\usepackage[margin=0.2in, paperwidth=9.8in, paperheight=4.6in]{geometry}\n")
 #   tls.append("\\geometry{a4paper,left=2mm,right=2mm}\n")
    tls.append("\\begin{document}\n")
    tls.append("\\noindent\n")

    tls.append("\\begin{tikzpicture}[x=0.5pt,y=0.5pt,every node/.style={inner sep=0,outer sep=0}]\n")

    for gene_name in ["rpoA", "rpoB", "rpoC"]:
        gene_len = gene_length[gene_name]
        tls.append("\\draw [color=black] ("+str(x_offset)+","+str(gene_shift[gene_name])+") -- ("+str(x_offset + gene_len)+","+str(gene_shift[gene_name])+");\n")
        tls.append("\\node [right,font={\\bf\\Large}] at (" + str(0) + "," + str(gene_shift[gene_name]-50) + ") {" + gene_name + "};\n")


        #podzialka
        for p in range(0, gene_len+99, 100):
            px = min(p, gene_len)
            x1 = x_offset + px
            y1 = gene_shift[gene_name]
            x2 = x_offset + px
            y2 = gene_shift[gene_name] - 14

            x1h = x_offset + px + 50
            y1h = gene_shift[gene_name]
            x2h = x_offset + px + 50
            y2h = gene_shift[gene_name] - 7
            if x1h <= x_offset + gene_len +2:
                tls.append("\\draw [color=black] ("+str(x1h)+","+str(y1h)+") -- ("+str(x2h)+","+str(y2h)+");\n")


            tls.append("\\draw [color=black] ("+str(x1)+","+str(y1)+") -- ("+str(x2)+","+str(y2)+");\n")
            if gene_len - px < 40 and gene_len != px:
                continue
#            if px == p:
            tls.append("\\node [below,font={\\bf\\Large}] at (" + str(px) + "," + str(gene_shift[gene_name]-20) + ") {" + str(px) + "};\n")

    x1 = x_offset + 426
    x2 = x_offset + 452
    y1 = gene_shift["rpoB"]

    # tls.append("\put("+str(x1)+","+str(y1)+"){\\color{red}\\vector(1,0){26}}")
    tls.append("\\draw [color=red, thick] ("+str(x1)+","+str(y1-10)+") -- ("+str(x1)+","+str(y1+10)+");\n")
    tls.append("\\draw [color=red, thick] ("+str(x2)+","+str(y1-10)+") -- ("+str(x2)+","+str(y1+10)+");\n")
    tls.append("\\draw [color=red, thick] ("+str(x1)+","+str(y1)+") -- ("+str(x2)+","+str(y1)+");\n")
    tls.append("\\draw [color=red, thick] ("+str(x1)+","+str(y1-1)+") -- ("+str(x2)+","+str(y1-1)+");\n")
    tls.append("\\draw [color=red, thick] ("+str(x1)+","+str(y1+1)+") -- ("+str(x2)+","+str(y1+1)+");\n")
    tls.append("\\node [color=red,below,font={\\bf\\Large},rotate=270] at (" + str(x1+23) + "," + str(y1-60) + ") {RRDR};\n")

    #pionowe kreski z mutacjami
    for source in comp_mutations:
        source_shift = source_shifts[source]
        color = source_color.get(source, "black")
        for gene_name in comp_mutations[source]:
            for (position, ref_state) in comp_mutations[source][gene_name]:
                x1 = x_offset + position
                y1 = gene_shift[gene_name] + source_shift
                x2 = x_offset + position
                y2 = gene_shift[gene_name] + source_shift + 10

                tl = "\\draw [color="+color+",semithick] ("+str(x1)+","+str(y1)+") -- ("+str(x2)+","+str(y2)+");\n"
                tls.append(tl)

                for (ref_state, ref_state_count) in comp_mutations[source][gene_name][(position, ref_state)]:
                    pass

    #legenda
    for source in comp_mutations:
        source_shift = source_shifts[source]
        color = source_color.get(source, "black")

        x1 = 600
        y1 = 10 + source_shift*3.0 + 2
        x2 = 600
        y2 = 10 + source_shift*3.0 + 12

        for ix in range(-2,3,1):
            tls.append("\\draw [color="+color+",thick] ("+str(x1+ix)+","+str(y1-5)+") -- ("+str(x2+ix)+","+str(y2+5)+");\n")
        tls.append("\\node [right,font={\\bf\\Large}] at (" + str(x1+30) + "," + str(y1+6) + ") {" + source.replace("_", " ") + "};\n")
    
  # tls.append(tl)

    tls.append("\\end{tikzpicture}\n")
    tls.append("\\end{document}\n")

    output_fn = comp_dir + "/tmp_comp_rpoB_figure.tex"

    output_fh = open(output_fn, "w")
    for tl in tls:
        output_fh.write(tl)
    output_fh.close()

    os.chdir(comp_dir)
    command = "pdflatex " + output_fn
    os.system(command)

    output1_fn = comp_dir + "/tmp_comp_rpoB_figure.pdf"
    output2_fn = comp_dir + "/comp_rpoB_figure.pdf"
    command = "pdfcrop --margins '3 3 3 3' " + output1_fn + " "+output2_fn
    os.system(command)

    command = "acroread " + output2_fn
    os.system(command)


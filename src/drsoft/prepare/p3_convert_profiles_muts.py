import sys
import os
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_utils,\
  gwamar_strains_io_utils, gwamar_res_io_utils, gwamar_res_utils,\
  gwamar_ann_utils, gwamar_muts_io_utils, gwamar_progress_utils,\
  gwamar_muts_utils

if __name__ == '__main__':
  print("p3_convert_profiles_muts.py: converts mutation profiles. Start.")
  gwamar_params_utils.overwriteParameters(sys.argv)
  parameters = gwamar_params_utils.readParameters()
  
  WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
  
  input_dir = parameters["DATASET_INPUT_DIR"]
  inter_dir = output_dir = parameters["DATASET_DIR"] + "/"+parameters["EP"]+"P/"
  gwamar_utils.ensure_dir(inter_dir)

  input_fh = open(input_dir + "/strains_ordered.txt")
  strains = gwamar_strains_io_utils.readStrains(input_fh)
  input_fh.close()

  strains_list = strains.allStrains()
      
  input_fh = open(inter_dir + "/res_profiles.txt")
  res_profiles_list = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
  input_fh.close()
  
  sc_strains = gwamar_res_utils.findScStrains(res_profiles_list)

  print("Strains selected as reference:")
  for strain_index in sc_strains:
    print(str(strain_index) + ": " +strains_list[strain_index])

  res_profiles = {}
  for res_profile in res_profiles_list:
    res_profiles[res_profile.drug_name] = res_profile
  
  if os.path.exists(input_dir + "/cluster_gene_ref_ids.txt"):
    cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
    cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh)
    cluster_fh.close()
  else:
    cluster_ids = {} 
    cluster_ids_rev = {}

  input_fh = open(input_dir + '/point_mutations.txt')
  gene_profiles = gwamar_muts_io_utils.readPointMutations(input_fh, cluster_ids=cluster_ids, cluster_ids_rev=cluster_ids_rev, gene_subset=None, strains_list=strains_list, m_type="a")
  input_fh.close()
  
  bin_profiles = {}
  bin_profiles_count = {}
  bin_profile_id = 0
  
  bin_mutations = set([])
  
  progress = gwamar_progress_utils.GWAMARProgress("Point mutations processed")
  progress.setJobsCount(len(gene_profiles))

  total_count = 0
          
  for gene_profile in gene_profiles.values():
    progress.update()
 
    for mutation in gene_profile.mutations.values():

      ref_state = gwamar_muts_utils.calcRefStateMutation(mutation.full_profile, sc_strains)
      if ref_state == None: continue
      
      total_count +=1

      bin_profile = gwamar_muts_utils.calcBinProfileMutation(mutation.full_profile, ref_state, gen_bin_profile=True)
      bin_profile_cmp = gwamar_muts_utils.compressProfile(bin_profile)
      mutation.ref_state = ref_state
      
      if not bin_profile_cmp in bin_profiles:
        bin_profiles[bin_profile_cmp] = bin_profile_id   
        bin_profiles_count[bin_profile_id] = 1   
        mutation.bin_profile_id = str(bin_profile_id)    
        bin_profile_id += 1
      else:
        bin_profile_id_ex = bin_profiles[bin_profile_cmp]
        mutation.bin_profile_id = str(bin_profile_id_ex)
        bin_profiles_count[bin_profile_id_ex] += 1


  print("mutation profiles count: ", total_count)
  print("bin profiles count: ", len(bin_profiles))

  output_fh = open(inter_dir + '/bin_profiles_P.txt', "w")
  for bin_profile_cmp in sorted(bin_profiles):
    bin_profile_id = bin_profiles[bin_profile_cmp]  
    tl = str(bin_profile_id) +"\t" + bin_profile_cmp
    if bin_profile_cmp.endswith(" "): continue
    output_fh.write(tl + "\n")
  output_fh.close()

  output_fh = open(inter_dir + '/bin_profiles_P_count.txt', "w")
  for bin_profile_id, bin_profile_count in bin_profiles_count.items():
    output_fh.write(str(bin_profile_id) + "\t" + str(bin_profile_count)+ "\n")
  output_fh.close()

  output_fh = open(inter_dir + '/bin_profiles_P_det.txt', "w")
  gwamar_muts_io_utils.writeMutationsHeader(output_fh, strains.allStrains(), only_strains=True, sep=" ")
  for gene_profile in sorted(gene_profiles.values(), key=lambda gp:(gp.gene_id, gp.cluster_id)):
    if gene_profile.ref_start == -1:
      tl = ">" + gene_profile.gene_id + "\t" + gene_profile.cluster_id + "\t" + gene_profile.gene_name + "\n"
    else:
      tl = ">" + gene_profile.gene_id + "\t"
      tl += gene_profile.cluster_id + "\t"
      tl += gene_profile.gene_name + "\t"
      tl += str(gene_profile.ref_start) + "\t"
      tl += str(gene_profile.ref_end) + "\t"
      tl += str(gene_profile.ref_strand) + "\n"
    output_fh.write(tl)
    
    if len(gene_profile.mutations) > 0:
      for position in sorted(gene_profile.mutations):
        mutation = gene_profile.mutations[position]
        ref_state = mutation.ref_state

        full_profile = mutation.full_profile
            
        desc_tl = gwamar_muts_utils.compressProfile(full_profile)
        if desc_tl.endswith(" "): continue

        tl = str(position) + "\t"
        tl += str(mutation.m_type) + "\t"
        tl += str(mutation.bin_profile_id) + "\t"
        tl += str(mutation.ref_state) + "\t"
        tl += desc_tl.strip() + "\n"
        output_fh.write(tl)

  output_fh.close()

  print("p3_convert_profiles_muts.py: Finished.")
    
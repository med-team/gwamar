import sys
import multiprocessing
from multiprocessing import Pool
from src.drsoft.utils import gwamar_params_utils, gwamar_comp_utils,\
  gwamar_utils, gwamar_res_io_utils, gwamar_progress_utils

def saveMutsPatterns(params):
    drug_name = params["DRUG_NAME"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    line0 = lines[0]
    dr_profile = line0.strip().split("\t")[0]
    

    dr_set = gwamar_comp_utils.getResSet(dr_profile)
    
    mutation_sets_list = [dr_set]
    mutation_desc_list = ["res"]
    
    
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        mut_desc = tokens[-1]
        mut_set = gwamar_comp_utils.getBinSet(bin_profile) & dr_set
        mutation_sets_list.append(mut_set)
        mutation_desc_list.append(mut_desc)
        if len(mut_set) <= 0: 
            break
    input_fh.close()
    
    n = len(mutation_sets_list)
    output_fh = open(analysis_comp_dir + drug_name + "/matrix.txt", "w")
    
    for i in range(n):
        output_fh.write(mutation_desc_list[i] + "\n")
    
    tl = "\t".join(["x"] + map(str, range(n))) + "\n"
    output_fh.write(tl)
    
    for i in range(n):
        set1 = mutation_sets_list[i]
        tl = str(i)
        for j in range(n):
            set2 = mutation_sets_list[j]
            tl += "\t" + str(len(set1 & set2))
        tl += "\n"
        
        output_fh.write(tl)
        
    return drug_name
    
if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    gwamar_utils.ensure_dir(analysis_dir)
    gwamar_utils.ensure_dir(analysis_comb_dir)
    gwamar_utils.ensure_dir(analysis_comp_dir)

    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        TASK["DRUG_NAME"] = drug_name

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("Combined")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveMutsPatterns, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveMutsPatterns(T)
            progress.update(str(r))


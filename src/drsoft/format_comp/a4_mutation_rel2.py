import sys
from multiprocessing import Pool
from src.drsoft.utils import gwamar_params_utils, gwamar_comp_utils,\
  gwamar_utils, gwamar_res_io_utils, gwamar_progress_utils

def saveMutsRelationsSep(params):
    drug_name = params["DRUG_NAME"]
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    analysis_comb_dir = parameters["RESULTS_A_COMB_S_SEL_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    input_fn = analysis_comb_dir + drug_name + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    line0 = lines[0]
    dr_profile = line0.strip().split("\t")[0]

    dr_set = gwamar_comp_utils.getResSet(dr_profile, inc_q=False)
    sc_set = gwamar_comp_utils.getSuscSet(dr_profile, inc_q=False)
    
    mutation_sets_list = []
    mutation_desc_list = []
    
    for line in lines[1:]:
        line = line.strip()
        tokens = line.split("\t")
        bin_profile = tokens[0]
        for mut_det_token in tokens[-1].split(";"):
            gene_id, cluster_id, gene_name, position_str, ref_state, mut_states = mut_det_token.split()
            position = int(position_str)
            if position < 0:
                continue
            mut_states_list = mut_states.split(",")

            mut_sets_map = gwamar_comp_utils.getBinSetsMap(bin_profile)
            for bin_id in mut_sets_map:
                mut_set = mut_sets_map[bin_id]
                if len(mut_set & dr_set) < len(mut_set & sc_set):
                    continue
                mutation_sets_list.append(mut_set)
                mut_desc = gene_id +" " +gene_name + " " + str(position) + " " + ref_state + " " + mut_states_list[int(bin_id)-1]
                mutation_desc_list.append(mut_desc)

    input_fh.close()
    
    n = len(mutation_sets_list)

    tls = []
    covered = set([])
    covered_map = set([])
    uncovered = set(range(n))

    for i in range(n):
        set1 = set(mutation_sets_list[i])
        for j in range(n):
            if i == j: continue
            set2 = set(mutation_sets_list[j])
            if set1.issubset(set2):
           # if set1.issubset(set2) and len(set1) < len(set2):
                covered.add(i)
                if i in uncovered:
                    uncovered.remove(i)
    
    for i in sorted(uncovered):
        tls.append(str(i) + "\n")

    for i in sorted(covered):
        set1 = set(mutation_sets_list[i])
        j_max = 0
        s_max = 0
        for j in uncovered:
            set2 = set(mutation_sets_list[j])

            if set1.issubset(set2):
                if len(set2) > s_max:
                    s_max = len(set2)
                    j_max = j
        if s_max > 0:
            tls.append(str(i)  + " " + str(j_max)+ "\n")

    for i in range(n):
        tl = str(i) + "\t" + mutation_desc_list[i] + "\n"          
        tls.append(tl)

    
    # for i in range(n):
    #     set1 = set(mutation_sets_list[i])
    #     opt_j = -1
    #     opt_j_size = 2000
    #     for j in range(n):
    #         if j == i: continue
    #         set2 = set(mutation_sets_list[j])
    #         if len(set1-set2) < 0.1*len(set1) and len(set2) < opt_j_size:
    #             opt_j_size = len(set2)
    #             opt_j = j
    #     if opt_j >= 0:
    #         text_line = str(i) + "\t" + str(opt_j) + "\t" + str(len(set1)) + "\t" + str(len(set1-mutation_sets_list[opt_j])) + "\t" + mutation_desc_list[i] + "\n"
    #     else:
    #         text_line = str(i) + "\t" + str(opt_j) + "\t" + str(len(set1)) + "\t" + str(len(set1-mutation_sets_list[0])) + "\t" + mutation_desc_list[i] + "\n"
        
    output_fh = open(analysis_comp_dir + drug_name + "/relations_sep.txt", "w")
    for tl in tls:   
        output_fh.write(tl)
    output_fh.close()

    return drug_name
    
if __name__ == '__main__':
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    analysis_dir = parameters["RESULTS_ANALYSIS_DIR"]
    analysis_comb_dir = parameters["RESULTS_A_COMB_S_DIR"]
    analysis_comp_dir = parameters["RESULTS_A_COMP_S_DIR"]
    
    gwamar_utils.ensure_dir(analysis_dir)
    gwamar_utils.ensure_dir(analysis_comb_dir)
    gwamar_utils.ensure_dir(analysis_comp_dir)

    TASKS = []
    TASK = {}
    
    input_fn = results_dir + "/res_profiles.txt"
    input_fh = open(input_fn)
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        TASK["DRUG_NAME"] = drug_name

        TASKS.append(TASK.copy())

    progress = gwamar_progress_utils.GWAMARProgress("saveMutsRelationsSep")
    progress.setJobsCount(len(TASKS))
        
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)
        for r in pool.imap(saveMutsRelationsSep, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = saveMutsRelationsSep(T)
            progress.update(str(r))


import sys
import os

from src.drsoft.utils import gwamar_tree_io_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils, gwamar_bin_utils, gwamar_scoring_flat_utils,\
  gwamar_scoring_tree_utils
from src.drsoft.modelling import tgh

def revOrder(score):
    rev_sorting = -1
    if score.startswith("pv-") or score.count("+") > 0:
        rev_sorting = 1
    return rev_sorting

def loadTreesList(strains_list, tree_id, score_full, results_dir):
    if score_full.startswith("r-"):
        input_fn = results_dir + "/tree" + tree_id + "_bin.txt"
    else:
        input_fn = results_dir + '/tree' + tree_id + '.txt'

    input_fh = open(input_fn)
    trees_list = gwamar_tree_io_utils.readPhyloTrees(input_fh, strains_list)
    input_fh.close()

    return trees_list

def computeScores(score_full, drug_name, parameters):
    input_dir = parameters["DATASET_INPUT_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    ranks_dir = parameters["RESULTS_RANKS_DIR"]

    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    strains_list = strains.allStrains()
    
    input_fh = open(results_dir + "/res_profiles.txt")
    res_profiles_map = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True, as_map = True)
    input_fh.close()
    drug_res_profile = res_profiles_map[drug_name]

    input_fn = results_dir + '/bin_profiles_' + parameters["MT"] + '.txt'
    if not os.path.exists(input_fn):
        print("File not found: " + input_fn)
        sys.exit(-1)
        
    #print("here", strains.count())
    gen_bin_profile = False
    input_fh = open(input_fn)
    bin_profiles = gwamar_bin_utils.readBinProfiles(input_fh, strains.count(), gen_bin_profiles=gen_bin_profile)
    input_fh.close()        
        
    rev_sorting = revOrder(score_full)
    if score_full.count("+") > 0:
        subscores = score_full.split("+")
        bin_profile_scores = gwamar_scoring_flat_utils.calcBinProfilesRanksSumScore(bin_profiles, drug_res_profile.drug_name, ranks_dir, subscores=subscores)
        return bin_profile_scores, rev_sorting

    score_tokens = score_full.split("_")
    score, score_params = score_tokens[0], score_tokens[1:]
    
    salpha = 0.0
    tree_id = parameters.get("TREE_ID", "")
    
    for score_param in score_params:
        if score_param.startswith("x"):
            salpha = float(score_param[1:])
            gen_bin_profile = True
        elif score_param.startswith("y"):
            tree_id = str(score_param[1:])

    if score == "mi":
        bin_profile_scores = gwamar_scoring_flat_utils.calcBinProfilesMI(bin_profiles, drug_res_profile)
    elif score == "lh":
        bin_profile_scores = gwamar_scoring_flat_utils.calcBinProfilesHyperTest(bin_profiles, drug_res_profile)
    elif score == "sp":
        bin_profile_scores = gwamar_scoring_flat_utils.calcBinProfilesSupports(bin_profiles, drug_res_profile, alpha=salpha)
    elif score == "or":
        bin_profile_scores = gwamar_scoring_flat_utils.calcBinProfilesOddsRatio(bin_profiles, drug_res_profile)
    elif score in ["tgh", "r-tgh","r-ntgh","ntgh"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)

        if score in ["r-ntgh","ntgh"]: norm = True
        else: norm=False
        mem_fn = parameters["TGH_DIR"] + "/"+drug_name+".txt"
        mem = tgh.readTGHMem(mem_fn)
        bin_profile_scores, mem = tgh.calcBinProfilesTGH(bin_profiles, drug_res_profile, trees_list, norm=norm, opt=True, w2=True, mem=mem)
        tgh.saveTGHMem(mem, mem_fn)

    elif score in ["mts", "r-mts"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)

        theta, zeta, beta = 0.5, 0.1, -1.0
        for score_param in score_params:
            if score_param.startswith("th"):
                theta = float(score_param[2:])
            elif score_param.startswith("ze"):
                zeta = float(score_param[2:])
            elif score_param.startswith("be"):
                beta = float(score_param[2:])
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesMinTreeScore(bin_profiles, drug_res_profile, trees_list, theta = theta, zeta=zeta, beta=beta, alpha=salpha)
    elif score in ["ts", "r-ts"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)

        theta, zeta, beta = 0.5, 0.0, -1.0
        for score_param in score_params:
            if score_param.startswith("th"):
                theta = float(score_param[2:])
            elif score_param.startswith("ze"):
                zeta = float(score_param[2:])
            elif score_param.startswith("be"):
                beta = float(score_param[2:])
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesTreeScore(bin_profiles, drug_res_profile, trees_list, theta = theta, zeta=zeta, beta=beta, alpha=salpha)
    elif score == "tsp":
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)
        r = len(drug_res_profile.getResistant())
        s = len(drug_res_profile.getSusceptible())
        beta = - float(r)/float(s)
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesTreeScore(bin_profiles, drug_res_profile, trees_list, beta=beta, alpha=salpha)
    elif score in ["ws", "r-ws"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesWeightedSupport(bin_profiles, drug_res_profile, trees_list, alpha=salpha, norm=False)
    elif score in ["pws", "r-pws"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesWeightedSupportPvalue(bin_profiles, drug_res_profile, trees_list, alpha=salpha, norm=False)
    elif score in ["nws", "r-nws"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesWeightedSupport(bin_profiles, drug_res_profile, trees_list, alpha=salpha, norm=True)
    elif score in ["sts", "r-sts"]:
        trees_list = loadTreesList(strains_list, tree_id, score_full, results_dir)
        theta, zeta, beta = 0.5, 0.0, -1.0
        eps_r, alpha_r, beta_r = 0.001, 0.2, 0.0001
        eps_m, psi_m, phi_m = 0.00001, 0.1, 0.0001
        for score_param in score_params:
            if score_param.startswith("th"):
                theta = float(score_param[2:])
            elif score_param.startswith("ze"):
                zeta = float(score_param[2:])
            elif score_param.startswith("be"):
                beta = float(score_param[2:])
            elif score_param.startswith("er"):
                eps_r = float(score_param[2:])
            elif score_param.startswith("ar"):
                alpha_r = float(score_param[2:])
            elif score_param.startswith("br"):
                beta_r = float(score_param[2:])
            elif score_param.startswith("em"):
                eps_m = float(score_param[2:])
            elif score_param.startswith("pm"):
                psi_m = float(score_param[2:])
            elif score_param.startswith("qm"):
                phi_m = float(score_param[2:])
                
        bin_profile_scores = gwamar_scoring_tree_utils.calcBinProfilesTreeScoreStoch(bin_profiles, drug_res_profile, trees_list, 
                                                            eps_m=eps_m, psi_m=psi_m, phi_m=phi_m,  
                                                            eps_r=eps_r, alpha_r=alpha_r, beta_r=beta_r, 
                                                            theta = theta, zeta=zeta, beta=beta, alpha=salpha)
    else:
        return None
    return bin_profile_scores, rev_sorting

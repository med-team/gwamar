from src.drsoft.structs.resistance_profile import ResistanceProfile
from src.drsoft.utils.gwamar_muts_utils import compressProfile, decompressProfile,convertFullProfile
from src.drsoft.utils.gwamar_muts_io_utils import writeMutationsHeader
from src.drsoft.utils.gwamar_strains_io_utils import strainAureusNames,\
    strainsMapRev

def saveResistanceProfiles(output_fh, res_profiles_list, strains_list, compress=False, header=True, sel_profiles=None):
    tls = []
    if compress == False:
        for drp in res_profiles_list:
            drug_name = drp.drug_name

            if sel_profiles != None and not drug_name in sel_profiles:
                continue
            tl = drug_name+"\t"
            for i in drp.getResistant():
                if i in drp.pred_strains:
                    tl += "*"
                tl += strains_list[i]+";"
            if tl.endswith(";"): tl = tl[:-1]
            tl += "\t"
            for i in drp.getSusceptible():
                if i in drp.pred_strains:
                    tl += "*"
                tl += strains_list[i]+";"
            if tl.endswith(";"): tl = tl[:-1]
            tl += "\t"
            for i in drp.getInterresistant():
                if i in drp.pred_strains:
                    tl += "*"
                tl += strains_list[i]+";"
            
            if tl.endswith(";"): tl = tl[:-1]
            tl += "\n"
            tls.append(tl)
    else:
        if header:
            writeMutationsHeader(output_fh, strains_subset=strains_list, only_strains=True, sep=" ")
        for drp in res_profiles_list:
            drug_name = drp.drug_name
            if sel_profiles != None and not drug_name in sel_profiles:
                continue
            cmp_profile = compressProfile(drp.full_profile)
            tl = drug_name + "\t" + cmp_profile + "\n"
            tls.append(tl)
        
    for tl in tls:
        output_fh.write(tl)
    return None

def readResistanceProfiles(input_fh, strains_list = None, load_int = False, as_map = False):
    res_profiles_list = []
    
    lines = input_fh.readlines()
    line0 = lines[0].strip()
    strain_ids_tmp = line0.split()
    strain_ids = []
    for strain_id_tmp in strain_ids_tmp:
        strain_ids.append(strainAureusNames(strain_id_tmp))
    n = len(strain_ids)

    if strains_list == None:
        strains_map_rev = None
    else:
        strains_map_rev = strainsMapRev(strains_list)
           
    # strains_list = line0.split()
    # n = len(strains_list)
    # strains_map_rev = strainsMapRev(strains_list)

    for line in lines[1:]:
        line = line.strip()
        if line.startswith("#"): continue
        tokens = line.split("\t")
        drug_name = tokens[0]
        if len(tokens) < 2: continue
        cmp_profile = tokens[1]

        full_profile_tmp = decompressProfile(cmp_profile, n)
        full_profile = convertFullProfile(full_profile_tmp, strain_ids, strains_map_rev)
        res_profile = ResistanceProfile(drug_name, len(full_profile))
        res_profile.full_profile = full_profile
        res_profile.computeResSets()
        res_profiles_list.append(res_profile)

    if as_map == True:
        res_profiles_map = {}
        
        for res_porfile in res_profiles_list:
            res_profiles_map[res_porfile.drug_name] = res_porfile
            
        return res_profiles_map
    
    return res_profiles_list


def readDrugNames(input_fh):
    drug_names = set([])
    lines= input_fh.readlines();
    
    for line in lines:
        if line.startswith("#"): continue
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2: continue
        drug_name = tokens[0]
        drug_names.add(drug_name)
    return drug_names

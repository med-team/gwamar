import sys
import os

#sys.path.append("../../")

parameters_map = {}

key_map = {"ACTION":"A", "a": "A",
           "WORKERS": "W", "W": "W", "w": "W",
           "DATASET": "D", "d": "D",
           "s":"SCORES", "S":"SCORES", "scores":"SCORES",
           "cs":"CSCORES", "CS": "CSCORES", "cscores": "CSCORES",
           "EXP_PREFIX": "EP", "e": "EP",
           "PERM_REP": "PR", "pr": "PR",
           "reps": "REPS",
           "MT": "MT", "mt": "MT",
           "OVERWRITE": "OW", "overwrite": "OW",
           "EXP_DIR":"ED", "ed": "ED",
           "SHOW_PROFILES": "SP", "sp":"SP",
           "subsets": "SUBSETS",
           "stats": "STATS"
           }

def shortKey(key_tmp):
    key = key_map.get(key_tmp, key_tmp)
    return key

def readParametersFromFile(input_fn, parameters_map):
    input_fh = open(input_fn);
    lines = input_fh.readlines()
    input_fh.close()
    for line in lines:
        line = line.strip().split("#")[0].strip()
        tokens = line.split(":")
        if len(tokens) < 2:
            continue
        key_tmp = tokens[0]
        key = shortKey(key_tmp)

        if key == "GWAMAR_PATH":
            pass
        else:
            value = ""
            for token in tokens[1:]:
                value += parameters_map.get(token, token)
            
            if not key in parameters_map:
                parameters_map[key] = value

    return parameters_map;

def getGWAMARPath(run_path, cmd_path):
    full_path = os.path.abspath(os.path.join(run_path, cmd_path))
    gwamar_path = os.path.dirname(full_path)

    while len(gwamar_path) > 1:
        path_tokens = os.path.split(gwamar_path)
        if path_tokens[1] == "gwamar":
            return gwamar_path + "/"
        gwamar_path = path_tokens[0]

    return gwamar_path;

def overwriteParameters(sys_argv):
    global parameters_map;
    
    run_path = os.path.abspath(os.curdir)
    cmd_path = sys_argv[0]
    gwamar_path = getGWAMARPath(run_path, cmd_path)
    os.chdir(gwamar_path)

    sys.path += ["src/"]
    
    parameters_map["GWAMAR_PATH"] = gwamar_path

    n = len(sys_argv)
    i = 1
    while i < n:
        arg = sys_argv[i]
        if arg.startswith("-"):
            if arg.count("=") == 1:
                key_tmp, value = arg[1:].split("=")
                i += 1
            else:
                key_tmp = arg[1:]
                if i < n-1: value = sys_argv[i+1]
                else: value = ""
                i += 2
        elif arg.count("=") == 1:
            key_tmp, value = arg.split("=")
            i += 1
        else:
            value = ""
            i += 1
        
        key = shortKey(key_tmp)
        parameters_map[key] = value
        
    return parameters_map


def readParameters(inc_tools = False):
    global parameters_map;
    config_dir = parameters_map["GWAMAR_PATH"] + "/config/";

    parameters_map = readParametersFromFile(config_dir + "config_params.txt", 
                                            parameters_map)
    if inc_tools:
      parameters_map = readParametersFromFile(config_dir + "config_tools.txt", 
                                              parameters_map)
    
    return parameters_map

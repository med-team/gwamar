import math

def bincoeff1(n, r):
    if r < n - r: r = n - r
    x = 1
    for i in range(n, r, -1): x *= i
    for i in range(n - r, 1, -1): x /= i
    return x

def hypergeometric(k, n1, n2, t):
    if t > n1 + n2: t = n1 + n2
    if k > n1 or k > t: return 0
    elif t > n2 and ((k + n2) < t): return 0
    else:
        c1 = math.log(bincoeff1(n1, k))
        c2 = math.log(bincoeff1(n2, t - k))
        c3 = math.log(bincoeff1(n1 + n2 ,t))
        
    return math.exp(c1 + c2 - c3)

def mean(data):
    n = len(data)
    if n==0: return 0.0
    sum1 = sum(data)
    
    return sum1/n

def binom(n, k):
    if n < k: return 0
    if k <= 0: return 1
    if 2*k>n: k = n-k
    ntok = 1
    for t in range(k):
        ntok = ntok*(n-t)//(t+1)
    return ntok

def hyperTestPvalue1(N,n,m,k,twoends=False):
    r = 0.0
    mian = binom(N,n)
    for kt in range(max(k,n+m-N),min(n+1,m+1),1):
        r += float(binom(m,kt)*binom(N-m,n-kt))
    res1 = r / mian
    if twoends:
        r = 0.0
        for kt in range(0,min(n+1,m+1,k+1),1):
            r += float(binom(m,kt)*binom(N-m,n-kt))
        res2 = r / mian
        return min(res1,res2)
    return res1

def hyperTestPvalue2(N,n,m,k,twoends=False):
    res1 = 0.0
    for kt in range(max(k,n+m-N),min(n+1,m+1),1):
        c1 = math.log(bincoeff1(m, kt))
        c2 = math.log(bincoeff1(N-m, n-kt))
        c3 = math.log(bincoeff1(N ,n))
        res1 += math.exp(c1 + c2 - c3)
    if twoends:
        res2 = 0.0
        for kt in range(0,min(n+1,m+1,k+1),1):
            c1 = math.log(bincoeff1(m, kt))
            c2 = math.log(bincoeff1(N-m, n-kt))
            c3 = math.log(bincoeff1(N ,n))
            res2 += math.exp(c1 + c2 - c3)
        return min(res1,res2)
    return res1

def hyperTestPvalue(N,n,m,k, twoends=False):
    if N <= 100: hyperTestPvalue1(N,n,m,k,twoends=twoends)
    return hyperTestPvalue2(N,n,m,k,twoends=twoends)

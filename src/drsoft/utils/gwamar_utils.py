import os
from src.drsoft.utils.gwamar_params_utils import parameters_map

def shift(textid, exp_len):
    ret = textid
    diff = exp_len - len(textid)
    ret += ''.join([" "]*diff)
    return ret;

def my_split(s, seps):
    res = [s]
    for sep in seps:
        s, res = res, []
        for seq in s: res += seq.split(sep)
    return res

def ensure_dir(path):
    if not os.path.exists(path):
        try:
            os.mkdir(path)
        except:
            pass

def logExecutionTime(time_sec, comment):
    output_dir = parameters_map["DATASET_DIR"]
    if not os.path.exists(output_dir):
        return None
    output_fn = output_dir + "exec_times.log"
    output_fh = open(output_fn, "a")
    output_fh.write(str(time_sec) + " " + str(comment) + "\n")
    output_fh.close()
    return None

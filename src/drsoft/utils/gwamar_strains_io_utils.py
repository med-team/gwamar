from src.drsoft.structs.strains import Strains

def strainAureusNames(strain_id):
    if strain_id.startswith("aureus_") and strain_id != "aureus_M1015":
        strain_id = strain_id[7:]
    return strain_id

def readStrains(input_fh):
    strains = Strains();
    lines = input_fh.readlines();
    count_pos = 0
    for line in lines:
        tokens = line.split()
        if line.startswith("#") or len(tokens) < 1: continue
        
        if len(tokens) == 1 or tokens[1] == "-":
            strain_id = tokens[0]
            strain_id = strainAureusNames(strain_id)
            
            strains.addStrain(strain_id, count_pos)
            count_pos += 1
        else: 
            strain_id = tokens[0]
            strain_id = strainAureusNames(strain_id)
            
            strains.addStrain(strain_id, count_pos)
            count_pos += 1
    return strains

def strainsMapRev(strains_list):
    strains_map_rev = {}
    n = len(strains_list)
    for i in range(n):
        strain_id = strainAureusNames(strains_list[i])
        strains_map_rev[strain_id] = i
    return strains_map_rev


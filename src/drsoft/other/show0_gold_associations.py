import sys
import os
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_utils,\
  gwamar_strains_io_utils, gwamar_res_io_utils, gwamar_bin_utils,\
  gwamar_ann_utils, gwamar_muts_io_utils

def printGoldShort(gold_assocs, comment=""):
    total = 0
    for drug_name in gold_assocs:
        tmp = gold_assocs[drug_name].keys()
        tl = comment+" " +drug_name +" "
        for gene_id in tmp:
            count = len(gold_assocs[drug_name][gene_id])
            total += count
            tl += gene_id +"("+str(count)+"),"

        print(tl.strip(","))
    print(comment +" total: " + str(total))

if __name__ == '__main__':
    print("a3_save_associations.py: saves association lists.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    dataset = parameters["D"]
    input_dir = parameters["DATASET_INPUT_DIR"]
    gold_dir = parameters["DATASET_GOLD_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    scores_dir = parameters["RESULTS_SCORES_DIR"]
    ranks_dir = parameters["RESULTS_RANKS_DIR"]
    cmp_dir = parameters["RESULTS_CMP_DIR"]

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    scores_p = parameters["SCORES"].split(",")


    gwamar_utils.ensure_dir(cmp_dir)
    gwamar_utils.ensure_dir(cmp_dir + "/assocs/")
    
    input_fh = open(input_dir + "/strains_ordered.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    m_type = parameters["MT"]
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_res_profiles = gwamar_res_io_utils.readResistanceProfiles(input_fh, load_int=True)
    input_fh.close()
    
    input_fh = open(results_dir + 'bin_profiles_' + m_type + '_det.txt')
    bin_profile_details = gwamar_bin_utils.readBinProfileDetails(input_fh, mut_type=m_type, out_format="tuple")
    input_fh.close()

    input_fh = open(results_dir + 'bin_profiles_' + m_type + '.txt')
    bin_profiles = gwamar_bin_utils.readBinProfiles(input_fh, strains.count(), gen_bin_profiles=True)
    input_fh.close()
    
    input_fn = input_dir + "/cluster_gene_ref_ids.txt"
    if os.path.exists(input_fn):
        cluster_fh = open(input_dir + "/cluster_gene_ref_ids.txt")
        cluster_ids, cluster_ids_rev = gwamar_ann_utils.readClusterGeneIDs(cluster_fh, gene_ref_ids=True)
        cluster_fh.close()
    else:
        cluster_ids = {} 
        cluster_ids_rev = {}

    input_fn = gold_dir + "/gold_assocs_H.txt"
    if not os.path.exists(input_fn):
        print("File does not exist:" + input_fn)
        drug_res_conf_db = {}
    else:
        input_fh = open(input_fn)
        drug_res_conf_db = gwamar_muts_io_utils.readGoldAssociations(input_fh, cluster_ids_rev)
        input_fh.close()
    
    input_fn = gold_dir + "/gold_assocs_A.txt"
    if not os.path.exists(input_fn):
        print("File does not exist:" + input_fn)
        drug_res_all_db = {}
    else:
        input_fh = open(input_fn)
        drug_res_all_db = gwamar_muts_io_utils.readGoldAssociations(input_fh, cluster_ids_rev)
        input_fh.close()

    score_params_union = set([])    
    drug_names_map = {}
    drug_name_id = 0
    for drug_res_profile in drug_res_profiles:
        drug_name = drug_res_profile.drug_name

        for score_fn in os.listdir(scores_dir + "/" + drug_name):
            input_fn = scores_dir + "/" + drug_name  + "/" + score_fn
            if os.path.exists(input_fn): 
                score_tmp = score_fn[:-4]
                if score_tmp in scores_p:
                    score_params_union.add(score_tmp)
                if not drug_name in drug_names_map:
                    drug_names_map[drug_name] = drug_name_id
                    drug_name_id += 1

    print(','.join(drug_names_map.keys()))
    printGoldShort(drug_res_all_db, "all")
    printGoldShort(drug_res_conf_db, "hc")
    
    print("show0_save_associations.py: Finished.")

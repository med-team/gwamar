import sys
import os
from multiprocessing import Pool
import multiprocessing
from src.drsoft.utils import gwamar_params_utils, gwamar_res_io_utils,\
  gwamar_bin_utils, gwamar_res_utils, gwamar_utils, gwamar_progress_utils

def scoreParamsSet(params):
    score = params["SCORE"]
    drug_name = params["DRUG_NAME"]
  
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()

    perm_scores_dir = parameters["RESULTS_PERM_SCORES_DIR"]
    perm_profiles_dir = parameters["RESULTS_PERM_PROFILES_DIR"]
    perm_ranks_dir = parameters["RESULTS_PERM_RANKS_DIR"]
    perm_rep = int(parameters["REPS"])
    
    rev_order = False
    if score.startswith("pv-") or score.count("+") > 0:
        rev_order = True

    input_fh = open(perm_profiles_dir + "/" + drug_name +".txt")
    drug_names_ids = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    input_fh = open(results_dir + '/bin_profiles_' + parameters["MT"] + '_count.txt')
    bin_profile_count = gwamar_bin_utils.readBinProfileCounts(input_fh)
    input_fh.close()
    
    for drug_name_id in drug_names_ids:
        
        current_id = gwamar_res_utils.drugNamePermID(drug_name_id)
        if current_id >= perm_rep: continue
        
        text_lines = []

        input_fn = perm_scores_dir + drug_name + "/"+score + "/" + drug_name_id +  ".txt"
        if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 10:
            print("NOT EXIST: ", input_fn)
            continue
        
        input_fh = open(input_fn)
        bin_profiles_sorted = gwamar_bin_utils.readBinProfilesSorted(input_fh, rev_order)
        input_fh.close()
        
        n = len(bin_profiles_sorted)
        bin_profiles_ranks = gwamar_bin_utils.computeRankings(bin_profiles_sorted, bin_profile_count)

        for i in range(n):
            bin_profile_id, _ = bin_profiles_sorted[i]
            min_ranks_pos, max_ranks_pos = bin_profiles_ranks[bin_profile_id][:2]
            text_line = str(bin_profile_id) + "\t"
            text_line += str(min_ranks_pos) + "\t"
            text_line += str(max_ranks_pos) + "\t"
            text_line += "\n"
            text_lines.append(text_line)
    
        output_fn = perm_ranks_dir + drug_name + "/" + score + "/" + drug_name_id + ".txt"    
        output_fh = open(output_fn, "w")
        for text_line in text_lines:
            output_fh.write(text_line)
        output_fh.close()
        
    return score

if __name__ == '__main__':
    print("perm3_save_perm_rep_rankings.py. Start.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    input_dir = parameters["DATASET_INPUT_DIR"]
    perm_dir = parameters["RESULTS_PERM_DIR"]
    results_dir = parameters["RESULTS_DIR"]
    perm_scores_dir = parameters["RESULTS_PERM_SCORES_DIR"]
    perm_ranks_dir = parameters["RESULTS_PERM_RANKS_DIR"]

    gwamar_utils.ensure_dir(perm_ranks_dir)
    
    score_params = parameters["SCORES"].split(",")
    
    TASKS = []
    TASK = {}
    
    input_fh = open(results_dir + "/res_profiles.txt")
    drug_names = gwamar_res_io_utils.readDrugNames(input_fh)
    input_fh.close()
    
    for drug_name in drug_names:
        gwamar_utils.ensure_dir(perm_ranks_dir + drug_name)
        for score_param in score_params:
            gwamar_utils.ensure_dir(perm_ranks_dir + drug_name + "/" + score_param)

            TASK["DRUG_NAME"] = drug_name
            TASK["SCORE"] = score_param
            
            TASKS.append(TASK.copy())

        
    progress = gwamar_progress_utils.GWAMARProgress("Rankings computed")
    progress.setJobsCount(len(TASKS))
    WORKERS = min(WORKERS, len(TASKS))
    
    if WORKERS > 1:
        pool = Pool(processes=WORKERS)   
        for r in pool.imap(scoreParamsSet, TASKS):
            progress.update(str(r))
    else:
        for T in TASKS:
            r = scoreParamsSet(T)
            progress.update(str(r))

            
    print("perm3_save_perm_rep_rankings.py. Finished.")

import sys
import os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_tree_io_utils

if __name__ == '__main__':
    print("t4_save_strains_ordered.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    phylo_dir = parameters["PREBROAD_TREE_DIR"]
    input_dir = parameters["PREBROAD_OUTPUT_DIR"]
  
    input_fh = open(input_dir + "/strains.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    output_fn = input_dir + "strains_ordered.txt"
    
    if os.path.exists(output_fn):
        print("File " + output_fn + " already exists. First remove it.")
        exit()
    
    input_fn = input_dir + 'tree.txt'
    
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        tree_line = input_fh.readline()
        input_fh.close()
        strains_list = gwamar_tree_io_utils.getTreeStrainList(tree_line)
    else:
        strains_list = strains.allStrains()

    output_fh = open(output_fn, "w")
    for strain_id in strains_list:
        output_fh.write(strain_id + "\n")
    output_fh.close()

    print("t4_save_strains_ordered.py. Finished.")
    
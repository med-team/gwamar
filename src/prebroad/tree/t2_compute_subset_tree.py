import sys
import os
import shutil
import platform
import multiprocessing

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils

def ensure_rem(filename):
    if os.path.exists(filename):
        os.remove(filename)

def checkToolPathExists(path):
  if not os.path.exists(path):
    print("Indicated path '" + path + "' to external tool does not exist.")
    print("Make sure your configuration in gwamar/config/config_tools.txt file is correct")
    print("Exiting.")
  sys.exit(-1)

def computeTreePHYLIP(phylip_path, phylip_exe, input_fn, output_fn, sufix = "_phylip"):
    abs_phylip_path = phylip_path
    if not os.path.exists(abs_phylip_path):
        print("Cannot find PHYLIP at this path: " + abs_phylip_path)
        sys.exit(-1)
    outtree_fn = "outtree"+sufix
    outfile_fn = "outfile"+sufix
    tmpfile_fn = "tmpfile"+sufix
    os.chdir(phylip_path)   
    ensure_rem(outfile_fn)
    ensure_rem(outtree_fn)
    tmp_fn = tmpfile_fn
    tmp_fh = open(tmp_fn, "w")
    tmp_fh.write(input_fn + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outfile_fn + "\n")
    tmp_fh.write("Y" + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outtree_fn + "\n")
    tmp_fh.close()
    if platform.system().count("WIN") > 0:
        os.system(abs_phylip_path + "dnamlk < " + tmp_fn)
    else:
        os.system(abs_phylip_path + "/dnamlk < " + tmp_fn)

    os.rename(outtree_fn, output_fn)

def computeTreePHYML(phyml_path, phyml_exe, input_fn, output_fn, sufix = "_phyml"):
    phyml_path = os.path.abspath(phyml_path)
    if not os.path.exists(phyml_path):
        print("Cannot find PHYML at this path: " + phyml_path)
        sys.exit(-1)
    os.chdir(phyml_path)


    input_tmp = "aln.txt"
    ensure_rem(input_tmp)

    shutil.copy2(input_fn, input_tmp)

    output1_fn = input_fn + "_phyml_stats.txt"
    ensure_rem(output1_fn)
    output2_fn = input_fn + "_phyml_tree.txt"
    ensure_rem(output2_fn)
    output_tmp = input_tmp + "_phyml_tree.txt"

    command = phyml_exe
    command += " -o n "
    command += "-i " + input_tmp
    os.system(command)
    os.rename(output_tmp, output_fn)
    return None

def computeConsensusRAXML(raxml_path, raxml_exe, output_fn,sufix = "TEST",raxml_type="MRE"):
    raxml_path = os.path.abspath(raxml_path)
    if not os.path.exists(raxml_path):
        print("Cannot find RAxML at this path: " + raxml_path)
        sys.exit(-1)
    os.chdir(raxml_path)
    
    output1_fn = "RAxML_info."+sufix+"2"
    ensure_rem(output1_fn)

    raxml_type = raxml_type.upper()
    if raxml_type == "MRE":
        output2_fn = "RAxML_MajorityRuleExtendedConsensusTree."+sufix+"2"
    elif raxml_type == "MR":
        output2_fn = "RAxML_MajorityRuleConsensusTree."+sufix+"2"
    elif raxml_type == "STRICT":
        output2_fn = "RAxML_StrictConsensusTree."+sufix+"2"
    else:
        output2_fn = "RAxML_MajorityRuleExtendedConsensusTree."+sufix+"2"
    ensure_rem(output2_fn)

    command = raxml_exe
    command += " -J "+raxml_type+" -z RAxML_bootstrap."+sufix +" -m GTRGAMMA -p 12345 -n "+sufix+"2"
    command += " -T " +str(2)

    print(command)
    os.system(command)

    print("h1", output2_fn)
    print("h2", output_fn)
    os.rename(output2_fn, output_fn)
    return None


def computeTreeRAXML(raxml_path, raxml_exe, input_fn, sufix="TEST", raxml_rep=100):
    raxml_path = os.path.abspath(raxml_path)
    if not os.path.exists(raxml_path):
        print("Cannot find RAxML at this path: " + raxml_path)
        sys.exit(-1)
    os.chdir(raxml_path)
    
    input_tmp = "aln.txt"
    ensure_rem(input_tmp)

    if raxml_exe.lower().count("threads") == 1:
        threads = min(multiprocessing.cpu_count(), 4)
    else:
        threads = 1

    shutil.copy2(input_fn, input_tmp)

    output1_fn = "RAxML_info."+sufix
    ensure_rem(output1_fn)

    output2_fn = "RAxML_bootstrap."+sufix
    ensure_rem(output2_fn)

    command = raxml_exe
    command += " -x 12345 -p 12345 -N "+str(raxml_rep)+" -m GTRGAMMA "
    if threads>1:
        command += "-T " +str(threads)
    command += " -s " + input_tmp +" -n "+ sufix
    os.system(command)
    return None

def computeTreePROML(input_fn, outfile_fn, outtree_fn, sufix = ""):
    if platform.system().count("WIN") > 0:
        phylip_path = "C:/Users/workshop/Dropbox/camber/phylip/exe"
        if platform.system().upper().count("CYGWIN") > 0:
            input_fn.replace("/cygdrive/c/", "C:/")
            outfile_fn.replace("/cygdrive/c/", "C:/")
            outtree_fn.replace("/cygdrive/c/", "C:/")
            abs_phylip_path = phylip_path        
    else:    
        phylip_path = "/home/misias/phylip/exe/"
        abs_phylip_path = os.path.abspath(phylip_path)
    os.chdir(phylip_path)
    ensure_rem(outfile_fn)
    ensure_rem(outtree_fn) 
    print(os.path.curdir, abs_phylip_path)
    print(input_fn)
    print(outfile_fn)
    print(outtree_fn)
    tmp_fn = "tmpfile" + sufix
    tmp_fh = open(tmp_fn, "w")
    tmp_fh.write(input_fn + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outfile_fn + "\n")
    tmp_fh.write("Y" + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outtree_fn + "\n")
    tmp_fh.close()
    if platform.system().count("dows") > 0:
        os.system("promlk < " + tmp_fn)
    else:
        os.system(abs_phylip_path + "/promlk < " + tmp_fn)

    return None

if __name__ == '__main__':
    print("t2_compute_subset_tree.py. Start.")

    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters(inc_tools = True)

    input_download_dir = parameters["PREBROAD_DOWNLOAD_DIR"]
    phylo_dir = parameters["PREBROAD_TREE_DIR"]
    input_dir = parameters["PREBROAD_OUTPUT_DIR"]
    m_type = parameters["PREBROAD_SUB"]
    phylip_path = parameters["PHYLIP_PATH"]
    tree_soft = parameters.get("DEFAULT_TREE_SOFT", "PHYML")

    input_fh = open(input_dir + "/strains.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    strains_ord = sorted(strains.allStrains())
    
    input_fn = phylo_dir + 'alignment_cluster_'+m_type+'.txt'
    output_fn = phylo_dir + 'tree_raw_'+tree_soft.lower()+".txt"


    if tree_soft.upper() == "RAXML":
        raxml_type = parameters.get("RAXML_TYPE", "MRE")
        raxml_rep = parameters.get("RAXML_REP","100")

        raxml_path = parameters["RAXML_PATH"]
        raxml_exe = parameters["RAXML_EXE"]
        
        checkToolPathExists(raxml_exe)
        computeTreeRAXML(raxml_path, raxml_exe, input_fn, raxml_rep=raxml_rep)
        computeConsensusRAXML(raxml_path, raxml_exe, output_fn,raxml_type=raxml_type)
    elif tree_soft.upper() == "PHYML":
        phyml_path = parameters["PHYML_PATH"]
        phyml_exe = parameters["PHYML_EXE"]
        checkToolPathExists(phyml_exe)
        computeTreePHYML(phyml_path, phyml_exe, input_fn, output_fn)
    elif tree_soft.upper() == "PHYLIP":
        phylip_path = parameters["PHYLIP_PATH"]
        phylip_exe = parameters["PHYLIP_EXE"]
        checkToolPathExists(phylip_exe)
        computeTreePHYLIP(phylip_path, phylip_exe, input_fn, output_fn)
    else:
        print("Unsupported phylogeny software: " + tree_soft)

    print("t2_compute_subset_tree.py. Finished.")



import sys,os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_tree_io_utils, gwamar_params_utils
from src.drsoft.structs import res_tree

thr = 0.000000001

def transformTree(tree, clusters_map):
    for leaf_node in tree.root.getLeaves():
        strains_tmp = list(clusters_map[leaf_node.label].split(" "))
        if len(strains_tmp) == 1:
            leaf_node.label = strains_tmp[0]
            leaf_node.node_id = strains_tmp[0]
        else:
            for strain_id in strains_tmp:
                new_leaf = res_tree.ResTreeNode(strain_id)
                new_leaf.e_len = 0.00001
                new_leaf.label = strain_id
                leaf_node.addChild(new_leaf)
    return tree

if __name__ == '__main__':
    print("t3_resolve_clusters.py. Start.")
    
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    m_type = parameters["PREBROAD_SUB"]
    phylo_dir = parameters["PREBROAD_TREE_DIR"]
    input_dir = parameters["PREBROAD_OUTPUT_DIR"]
    
    tree_soft = parameters["DEFAULT_TREE_SOFT"]

    cluster_strains = []
    clusters_map = {}
    input_fh = open(phylo_dir + "/clusters_map_all.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        clusters_map[tokens[0]] = tokens[1].replace(" ", ",")
        cluster_strains.append(tokens[0])
    input_fh.close()
    
    input_fn = phylo_dir + 'tree_raw_'+tree_soft.lower()+".txt"
    output_fn = phylo_dir + 'tree'+tree_soft.upper()+".txt"
    input_fh = open(input_fn)
    output_fh = open(output_fn, "w")
    trees = gwamar_tree_io_utils.readPhyloTrees(input_fh, strains_list=cluster_strains, strains_map = {})
    tree = trees[0]
    tree = transformTree(tree, clusters_map)
    flat_tree = tree.flatten(thr)
    text_line = flat_tree.toString(lengths=False, strains=None, int_labels=False) + ";\n"
    output_fh.write(text_line);
    output_fh.close();
    input_fh.close()

    print("t3_resolve_clusters.py. Finished.")
    
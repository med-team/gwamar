import sys,os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_strains_io_utils,\
  gwamar_res_io_utils
from src.drsoft.structs import resistance_profile

profile_clusters = {
"Fluoroquinolones":["Gatifloxacin","Ofloxacin","Moxifloxacin","Levofloxacin","Ciprofloxacin"],
"Aminoglycosides":["Amikacin","Kanamycin","Capreomycin"],
"Rifampicin":["Rifampicin","Rifabutin"]
 }  

if __name__ == '__main__':
    print("p4_combine_resistance_profiles.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    input_dir = parameters["PREBROAD_DIR"]
    output_dir = parameters["PREBROAD_OUTPUT_DIR"]

    input_fh = open(output_dir + "/strains.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()
    
    strains_list = strains.allStrains()
    k = len(strains_list)
    
    input_fh = open(output_dir + "/res_profiles_ret.txt")
    res_profiles = gwamar_res_io_utils.readResistanceProfiles(input_fh, strains_list=strains.allStrains(), load_int=True)
    input_fh.close()
    

    res_profiles_map = {}
    for drp in res_profiles: res_profiles_map[drp.drug_name] = drp

    print(res_profiles_map.keys())
    res_profiles_comb = []

    for cluster_id in profile_clusters:
        cluster_drp = resistance_profile.ResistanceProfile(cluster_id, count = k)

        for drug_name in profile_clusters[cluster_id]:
            drp = res_profiles_map.get(drug_name, None)
            if drp!= None:
                print(''.join(drp.full_profile), drug_name)

        for i in range(k):
            res_count = 0
            int_count = 0
            susc_count = 0
            for drug_name in profile_clusters[cluster_id]:
                drp = res_profiles_map.get(drug_name, None)

                if drp != None:

                    if drp.full_profile[i] == "R": res_count += 1
                    elif drp.full_profile[i] == "I": int_count += 1
                    elif drp.full_profile[i] == "S": susc_count += 1

            if res_count > 0 and susc_count >0:
              #  print("conflict", cluster_id, profile_clusters[cluster_id], i, strains_list[i], susc_count, int_count, res_count)
                cluster_drp.changeState(i, "?")
            elif res_count >= 1 and susc_count == 0:
                cluster_drp.changeState(i, "R")
            elif res_count == 0 and susc_count >= 1:
                cluster_drp.changeState(i, "S")
            elif res_count == 0 and susc_count == 0 and int_count >=1:
                cluster_drp.changeState(i, "I")
            else:
                cluster_drp.changeState(i, "?")
        print(''.join(cluster_drp.full_profile), cluster_drp.drug_name)
        res_profiles_comb.append(cluster_drp)

    for drp in res_profiles_comb:
        drp.computeResSets()
        print(drp.drug_name, len(drp.getKnown()))

  #  for drp in 

    output_fh = open(output_dir + "/res_profiles.txt", "w")
    gwamar_res_io_utils.saveResistanceProfiles(output_fh, res_profiles_comb, strains_list, compress=True)
    output_fh.close()
#    
    print("p4_combine_resistance_profiles.py. Finished.")

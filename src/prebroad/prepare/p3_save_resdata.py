import sys,os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_res_io_utils, gwamar_params_utils,\
  gwamar_strains_io_utils
from src.drsoft.structs import resistance_profile

def readResProfilesFromTable(input_fh):
    lines = input_fh.readlines()
    line0 = lines[0]
    tokens = line0.split()
    drug_names = tokens[1:]
    
    res_profiles_map = {}
    for drug_name_tmp in drug_names:
        drug_name = drug_names_map.get(drug_name_tmp, drug_name_tmp)
        res_profile = resistance_profile.ResistanceProfile(drug_name, strains.count())
        res_profiles_map[drug_name] = res_profile
        
    for line in lines[1:]:
        line = line.strip()
        tokens_tmp = line.split()
        if not len(tokens_tmp) == len(drug_names) + 1:
            tokens_tmp = line.split("\t")
        strain_id = tokens_tmp[0]
        strain_index = strains.getPosition(strain_id)
        res_tokens = tokens_tmp[1:]
   #     print(strain_id, strain_index, res_tokens)
        
        for i in range(len(res_tokens)):
            drug_name_tmp = drug_names[i]
            drug_name = drug_names_map.get(drug_name_tmp, drug_name_tmp)
            res_type_tmp = res_tokens[i].upper()
            res_profile = res_profiles_map[drug_name]
            if res_type_tmp == "": res_type_tmp = "?"
            elif res_type_tmp == "U": res_type_tmp = "?"
            elif res_type_tmp == "?": res_type_tmp = "?"
                
            res_profile.full_profile[strain_index] = res_type_tmp

    return res_profiles_map

if __name__ == '__main__':
    print("p3_save_resdata.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
    
    input_dir = parameters["PREBROAD_DIR"]
    input_download_dir = parameters["PREBROAD_DOWNLOAD_DIR"]
    output_dir = parameters["PREBROAD_OUTPUT_DIR"]

    input_fh = open(output_dir + "/strains.txt")
    strains = gwamar_strains_io_utils.readStrains(input_fh)
    input_fh.close()

    strains_list = strains.allStrains()
    
    drug_names_map = {}
    input_fh = open(input_download_dir + "GTBDR_120516.drugresistance.csv")
    lines = input_fh.readlines()
    input_fh.close()
    line0 = lines[0]

    tokens = line0.split()
    
    input_fh = open(input_download_dir + "GTBDR_120516.drugresistance.csv")
    res_profiles_map = readResProfilesFromTable(input_fh)
    input_fh.close()

    res_profiles_list = list(res_profiles_map.values())
    
    output_fh = open(output_dir + "/res_profiles_ret.txt", "w")
    gwamar_res_io_utils.saveResistanceProfiles(output_fh, res_profiles_list, strains_list, compress=True, header=True)
    output_fh.close()

    print("p3_save_resdata.py. Finished.")

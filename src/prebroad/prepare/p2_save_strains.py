import sys,os

sys.path += [os.path.abspath(os.curdir)]

from src.drsoft.utils import gwamar_params_utils, gwamar_utils

if __name__ == '__main__':
    print("p2_save_strains.py. Start.")
    gwamar_params_utils.overwriteParameters(sys.argv)
    parameters = gwamar_params_utils.readParameters()
  
    input_dir = parameters["PREBROAD_DIR"]
    input_download_dir = parameters["PREBROAD_DOWNLOAD_DIR"]
    output_dir = parameters["PREBROAD_OUTPUT_DIR"]
    gwamar_utils.ensure_dir(input_dir)
    gwamar_utils.ensure_dir(input_download_dir)
    gwamar_utils.ensure_dir(output_dir)

    strain_list = []
    
    input_fn = input_download_dir + "/GTBDR_120516.drugresistance.csv"
    input_fh = open(input_fn)
    for line in input_fh.readlines()[1:]:
        tokens = line.split()
        if len(tokens) > 0:
            strain_list.append(tokens[0])
    input_fh.close()

    output_fh = open(output_dir + "/strains.txt", "w")
    for strain_id in strain_list:
        output_fh.write(strain_id + "\n")
    output_fh.close()
    print("p2_save_strains.py. Finished.")
    